﻿using System;
using System.Windows.Forms;

namespace Buscaminas {
    public partial class NuevoResultado : Form {
        private long segundos;

        public NuevoResultado( long s ) {
            InitializeComponent();
            InicializarVentana();
            segundos = s;
        }

        public void InicializarVentana() {
            string idioma = MinesWeeper.idioma;
            string nivel = MinesWeeper.nivel;

            if (idioma == "Spanish") {

                string nivelUsuario = "";
                if (nivel == "Easy") {
                    nivelUsuario = "Principiante";
                } else if (nivel == "Regular") {
                    nivelUsuario = "Intermedio";
                } else if (nivel == "Hard") {
                    nivelUsuario = "Experto";
                }

                Text = "Nuevo Record";
                labelTexto.Text = "Has hecho el mejor tiempo en el nivel " + nivelUsuario + ". Escribe tu nombre.";

                textBoxName.Text = "Anónimo";
                buttonOK.Text = "Aceptar";

            } else if (idioma == "English") {

                string nivelUsuario = "";
                if (nivel == "Easy") {
                    nivelUsuario = "Beginner";
                } else if (nivel == "Regular") {
                    nivelUsuario = "Medium";
                } else if (nivel == "Hard") {
                    nivelUsuario = "Expert";
                }

                Text = "New Record";
                labelTexto.Text = "You did the best time in the level " + nivelUsuario + ". Write your name.";

                textBoxName.Text = "Anonymous";
                buttonOK.Text = "OK";

            }

        }

        public static void ComprobarPartida( string nivel, long segundos ) {
            if (MinesWeeper.haHabidoTrucos) {
                MensajeTramposo();
                return;
            }

            string resultado = Program.LeerRegistro(nivel, "Null");

            if (resultado == "Null") {
                
                NuevoResultado nuevoResultado = new NuevoResultado(segundos);
                nuevoResultado.ShowDialog();

            } else {
                long segundosRegistro = Convert.ToInt64(resultado);
                
                if (segundos < segundosRegistro) {
                    NuevoResultado nuevoResultado = new NuevoResultado(segundos);
                    nuevoResultado.ShowDialog();
                } else {
                    MensajeGanador();
                }
                
            }
            

        }

        private void buttonOK_Click( object sender, System.EventArgs e ) {
            string jugador = textBoxName.Text;
            string nivel = MinesWeeper.nivel;
            string stringsec = segundos.ToString();

            if (jugador == "") {
                jugador = "Null";
            }

            Program.EscribirRegistro(nivel, stringsec);
            Program.EscribirRegistro( "Name"+nivel, jugador );

            Close();
        }

        public static void MensajeTramposo() {
            string idioma = MinesWeeper.idioma;

            if (idioma == "English") {
                MessageBox.Show("You are a Cheat.", "Game Over", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            } else if (idioma == "Spanish") {
                MessageBox.Show("Has usado trucos.", "Partida Terminada", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

        }

        public static void MensajeGanador() {
            string idioma = MinesWeeper.idioma;

            if (idioma == "English") {
                MessageBox.Show("Congratulations, you win.", "Game Over", MessageBoxButtons.OK, MessageBoxIcon.Information);
            } else if (idioma == "Spanish") {
                MessageBox.Show("Enhorabuena, has ganado.", "Partida Terminada", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

    }
}
