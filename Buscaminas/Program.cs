using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace Buscaminas
{
    static class Program
    {
        /// <summary>
        /// Claves acceso Registro
        /// </summary>
        private static readonly string appName = "MinesWeeperMario";
        public static readonly string seccionIdioma = "Language";
        public static readonly string seccionEstilo = "Style";
        public static readonly string seccionNivel = "Level";

        public static string estilo, nivel;
        private static string idioma;

        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {

            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            IdiomaRegistro();
            if (idioma != null)
            {
                MinesWeeper mw = new MinesWeeper(idioma, estilo, nivel);
                Application.Run(mw);
            }

        }

        public static void EscribirRegistro(string clave, string valor)
        {

            Microsoft.Win32.RegistryKey rkNuevo;
            rkNuevo = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(@"Software\" + appName);
            rkNuevo.SetValue(clave, valor);

        }

        public static bool ExisteRegistro()
        {

            Microsoft.Win32.RegistryKey rk;
            rk = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"Software\" + appName);

            return rk != null;
        }

        public static String LeerRegistro(string clave, string defecto)
        {

            Microsoft.Win32.RegistryKey rk;
            rk = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"Software\" + appName);

            object dato = rk.GetValue(clave);
            if (dato == null)
            {
                EscribirRegistro(clave, defecto);
                return defecto;
            }
            else
            {
                return dato.ToString();
            }
        }

        private static void IdiomaRegistro()
        {

            Microsoft.Win32.RegistryKey rk;
            rk = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"Software\" + appName);

            if (ExisteRegistro())
            {
                idioma = LeerRegistro(seccionIdioma, "English");
                estilo = LeerRegistro(seccionEstilo, "Normal");
                nivel = LeerRegistro(seccionNivel, "Easy");
            }
            else
            {
                SelectLanguage sl = new SelectLanguage();
                Application.Run(sl);

                if (sl.language == null)
                {
                    return;
                }

                idioma = sl.language;
                estilo = "Normal";
                nivel = "Easy";

                EscribirRegistro(seccionIdioma, sl.language);
                EscribirRegistro(seccionEstilo, "Normal");
                EscribirRegistro(seccionNivel, "Easy");
            }

        }

    }

}
