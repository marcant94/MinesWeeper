﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Buscaminas {

    public partial class MinesWeeper : Form {
        public System.ComponentModel.ComponentResourceManager resources;

        public static string idioma, estilo, nivel;
        public static bool haHabidoTrucos;

        public int filas, columnas, minas, altoCasilla, anchoCasilla, minasRestantes, celdasDescubiertas, celdasNoMinas;
        public bool primerClick, finalizado, trucos, trucosSecuencia;
        public long segundos;
        private Keys ultimaLetra;

        public Celda[][] tabMinas;

        public MinesWeeper( string idiomaUsuario, string estiloUsuario, string nivelUsuario ) {
            idioma = idiomaUsuario;
            estilo = estiloUsuario;
            nivel = nivelUsuario;
            trucos = false;
            InitializeComponent();

            resources = new System.ComponentModel.ComponentResourceManager(typeof(MinesWeeper));

            NuevaPartida();
            SetLanguage();
            SetNivel();            
        }

        public void SetEstilo() {

            if (MinesWeeper.estilo == "Basic") {
                for (int i = 0; i < filas; i++) {
                    for (int j = 0; j < columnas; j++) {
                        tabMinas[i][j].FlatStyle = FlatStyle.Flat;
                    }
                }
                menuEstiloNormal.CheckState = System.Windows.Forms.CheckState.Unchecked;
                menuEstiloBasico.CheckState = System.Windows.Forms.CheckState.Checked;
            } else if (MinesWeeper.estilo == "Normal") {
                for (int i = 0; i < filas; i++) {
                    for (int j = 0; j < columnas; j++) {
                        tabMinas[i][j].FlatStyle = FlatStyle.Standard;
                    }
                }
                menuEstiloNormal.CheckState = System.Windows.Forms.CheckState.Checked;
                menuEstiloBasico.CheckState = System.Windows.Forms.CheckState.Unchecked;
            }

        }

        public void SetLanguage() {

            if (idioma == "Spanish") {

                Text = "Buscaminas .NET 5 por Mario Cantelar";
                menuJuego.Text = "&Juego";
                menuFacil.Text = "&Principiante";
                menuIntermedio.Text = "&Intermedio";
                menuDificil.Text = "&Experto";
                menuMejoresTiempos.Text = "&Mejores Tiempos";
                menuSalir.Text = "&Salir";
                menuNuevaPartida.Text = "&Nueva Partida";
                menuIdioma.Text = "&Idioma";
                menuEspañol.Text = "&Español";
                menuIngles.Text = "&Inglés";
                reiniciarBoton.Text = "Reiniciar";
                menuEstiloBasico.Text = "&Básico";
                menuEstiloNormal.Text = "&Normal";
                menuEstilo.Text = "&Estilo";

                menuFacil.ToolTipText = "Cuadrícula 9 x 9, 16 minas";
                menuIntermedio.ToolTipText = "Cuadrícula 12 x 12, 28 minas";
                menuDificil.ToolTipText = "Cuadrícula 16 x 16, 51 minas";

                menuEspañol.CheckState = System.Windows.Forms.CheckState.Checked;
                menuIngles.CheckState = System.Windows.Forms.CheckState.Unchecked;

            } else if (idioma == "English") {

                Text = "MinesWeeper .NET by Mario Cantelar";
                menuJuego.Text = "&Game";
                menuFacil.Text = "&Beginner";
                menuIntermedio.Text = "&Medium";
                menuDificil.Text = "&Expert";
                menuMejoresTiempos.Text = "&Best Times";
                menuSalir.Text = "&Exit";
                menuNuevaPartida.Text = "&New Game";
                menuIdioma.Text = "&Language";
                menuEspañol.Text = "&Spanish";
                menuIngles.Text = "&English";
                reiniciarBoton.Text = "Restart";
                menuEstiloBasico.Text = "&Basic";
                menuEstiloNormal.Text = "&Normal";
                menuEstilo.Text = "&Style";

                menuFacil.ToolTipText = "Grid 9 x 9, 16 mines";
                menuIntermedio.ToolTipText = "Grid 12 x 12, 28 mines";
                menuDificil.ToolTipText = "Grid 16 x 16, 51 mines";

                menuEspañol.CheckState = System.Windows.Forms.CheckState.Unchecked;
                menuIngles.CheckState = System.Windows.Forms.CheckState.Checked;

            }

        }

        public void NuevaPartida() {
            haHabidoTrucos = trucos;
            panelTablero.Controls.Clear();
            SetNivel();            
            ConfigurarTablero();
            RellenarTablero();
            SetEstilo();
        }

        private void ActivarTrucos() {
            haHabidoTrucos = true;
            if (primerClick) {
                return;
            }

            for (int i = 0; i < filas; i++) {
                for (int j = 0; j < columnas; j++) {
                    Celda c = tabMinas[i][j];

                    if (c.esMina) {
                        if (trucos) {
                            c.Cursor = System.Windows.Forms.Cursors.No;
                        } else {
                            c.Cursor = System.Windows.Forms.Cursors.Hand;
                        }
                    }

                }
            }

        }

        public void SetNivel() {

            if (nivel == "Easy") {
                filas = 9;
                columnas = 9;
                minas = 16;

                menuFacil.CheckState = System.Windows.Forms.CheckState.Checked;
                menuIntermedio.CheckState = System.Windows.Forms.CheckState.Unchecked;
                menuDificil.CheckState = System.Windows.Forms.CheckState.Unchecked;
            } else if (nivel == "Regular") {
                filas = 12;
                columnas = 12;
                minas = 28;

                menuFacil.CheckState = System.Windows.Forms.CheckState.Unchecked;
                menuIntermedio.CheckState = System.Windows.Forms.CheckState.Checked;
                menuDificil.CheckState = System.Windows.Forms.CheckState.Unchecked;
            } else if (nivel == "Hard") {
                filas = 16;
                columnas = 16;
                minas = 51;

                menuFacil.CheckState = System.Windows.Forms.CheckState.Unchecked;
                menuIntermedio.CheckState = System.Windows.Forms.CheckState.Unchecked;
                menuDificil.CheckState = System.Windows.Forms.CheckState.Checked;
            }

            //minas = filas * columnas * 2 / 10;

        }

        public void CambiarMarcador() {

            int minasMarcadas = minas - minasRestantes;
            if (minasMarcadas <= minas) {
                progressBarMinas.Value = minasMarcadas;
            } else {
                progressBarMinas.Value = minas;
            }

            minasRestantesLabel.Text = "" + minasRestantes;

        }

        public void EstablecerTamañoCeldas() {
            altoCasilla = panelTablero.Size.Height / filas;
            anchoCasilla = panelTablero.Size.Width / columnas;
        }

        public void ConfigurarTablero() {
            primerClick = true;
            finalizado = false;

            minasRestantes = minas;
            progressBarMinas.Maximum = minas;
            celdasDescubiertas = 0;
            celdasNoMinas = ( filas * columnas ) - minas;

            segundos = 0;
            tiempoLabel.Text = "---";

            CambiarMarcador();
            EstablecerTamañoCeldas();
        }

        public void RellenarTablero() {
            tabMinas = new Celda[filas][];

            int acc = 0;
            for (int i = 0; i < filas; i++) {
                tabMinas[i] = new Celda[columnas];
                for (int j = 0; j < columnas; j++) {
                    Celda casilla = GenerarCasilla(i, j, acc++);
                    tabMinas[i][j] = casilla;
                    panelTablero.Controls.Add(casilla);
                }
            }

        }

        private Celda GenerarCasilla( int i, int j, int index ) {
            Celda casilla = new Celda(i, j);

            casilla.Name = "C-" + index;
            casilla.Size = new System.Drawing.Size(anchoCasilla, altoCasilla);
            casilla.Margin = new System.Windows.Forms.Padding(0);

            casilla.MouseDown += new MouseEventHandler(Casilla_Click);

            return casilla;
        }

        public void SumarAlrededor( int i, int j ) {

            if (( i - 1 >= 0 ) && ( j - 1 >= 0 )) {
                tabMinas[( i - 1 )][( j - 1 )].MinaAlrededor();
            }
            if (i - 1 >= 0) {
                tabMinas[( i - 1 )][j].MinaAlrededor();
            }
            if (( i - 1 >= 0 ) && ( j + 1 < columnas )) {
                tabMinas[( i - 1 )][( j + 1 )].MinaAlrededor();
            }
            if (j - 1 >= 0) {
                tabMinas[i][( j - 1 )].MinaAlrededor();
            }
            if (j + 1 < columnas) {
                tabMinas[i][( j + 1 )].MinaAlrededor();
            }
            if (( i + 1 < filas ) && ( j - 1 >= 0 )) {
                tabMinas[( i + 1 )][( j - 1 )].MinaAlrededor();
            }
            if (i + 1 < filas) {
                tabMinas[( i + 1 )][j].MinaAlrededor();
            }
            if (( i + 1 < filas ) && ( j + 1 < columnas )) {
                tabMinas[( i + 1 )][( j + 1 )].MinaAlrededor();
            }

        }

        public void RepartirMinas( Celda accionadora ) {
            int minasPorActivar = minas;
            Random rnd = new Random();

            while (minasPorActivar > 0) {
                int altoMina = rnd.Next(filas);
                int anchoMina = rnd.Next(columnas);

                Celda c = tabMinas[altoMina][anchoMina];

                if (!c.esMina && c != accionadora) {

                    bool noEstaAlrededor = Math.Abs(accionadora.x - altoMina) > 1 || Math.Abs(accionadora.y - anchoMina) > 1;

                    if (noEstaAlrededor) {
                        c.esMina = true;
                        //c.Text = "M";
                        minasPorActivar--;
                        SumarAlrededor(altoMina, anchoMina);
                    }

                }

            }

        }

        public void Revelar( int i, int j ) {
            Celda celda = tabMinas[i][j];

            if (celda.EsBandera() || !celda.estaActivada) {
                return;
            }
            celdasDescubiertas++;

            celda.PulsarCelda();

            if (celda.minasAlrededor != 0) {
                celda.Text = "" + celda.minasAlrededor;

                switch (celda.minasAlrededor) {
                    case 1:
                        celda.ForeColor = Color.RoyalBlue;
                        break;
                    case 2:
                        celda.ForeColor = Color.LimeGreen;
                        break;
                    case 3:
                        celda.ForeColor = Color.Red;
                        break;
                    case 4:
                        celda.ForeColor = Color.DarkOrange;
                        break;
                    case 5:
                        celda.ForeColor = Color.SlateBlue;
                        break;
                    default:
                        celda.ForeColor = Color.Black;
                        break;
                }

            }

        }

        public void RevelarAlrededor( int i, int j ) {
            if (tabMinas[i][j].EsBandera()) {
                return;
            }
            if (tabMinas[i][j].minasAlrededor != 0) {
                return;
            } else {
                Revelar(i, j);
            }

            if (( i > 0 ) && ( j > 0 ) && ( tabMinas[( i - 1 )][( j - 1 )].estaActivada )) {
                Revelar(i - 1, j - 1);
                RevelarAlrededor(i - 1, j - 1);
            }
            if (( i > 0 ) && ( tabMinas[( i - 1 )][j].estaActivada )) {
                Revelar(i - 1, j);
                RevelarAlrededor(i - 1, j);
            }
            if (( i > 0 ) && ( j < columnas - 1 ) && ( tabMinas[( i - 1 )][( j + 1 )].estaActivada )) {
                Revelar(i - 1, j + 1);
                RevelarAlrededor(i - 1, j + 1);
            }
            if (( j > 0 ) && ( tabMinas[i][( j - 1 )].estaActivada )) {
                Revelar(i, j - 1);
                RevelarAlrededor(i, j - 1);
            }
            if (( j < columnas - 1 ) && ( tabMinas[i][( j + 1 )].estaActivada )) {
                Revelar(i, j + 1);
                RevelarAlrededor(i, j + 1);
            }
            if (( i < filas - 1 ) && ( tabMinas[( i + 1 )][j].estaActivada )) {
                Revelar(i + 1, j);
                RevelarAlrededor(i + 1, j);
            }
            if (( i < filas - 1 ) && ( j > 0 ) && ( tabMinas[( i + 1 )][( j - 1 )].estaActivada )) {
                Revelar(i + 1, j - 1);
                RevelarAlrededor(i + 1, j - 1);
            }
            if (( i < filas - 1 ) && ( j < columnas - 1 ) &&
              ( tabMinas[( i + 1 )][( j + 1 )].estaActivada )) {
                Revelar(i + 1, j + 1);
                RevelarAlrededor(i + 1, j + 1);
            }

        }

        public void FinalizarPartida( bool ganador ) {
            finalizado = true;
            minasRestantesLabel.Text = "---";

            for (int i = 0; i < filas; i++) {
                for (int j = 0; j < columnas; j++) {
                    Celda celda = tabMinas[i][j];

                    if (celda.estaActivada) {
                        celda.Enabled = false;
                    }

                    if (celda.esMina) {

                        if (ganador) {

                            if (!celda.EsBandera()) {
                                PonerMina(celda);
                            }

                        } else {


                            if (!celda.EsBandera()) {
                                celda.BackColor = Color.Crimson;
                                PonerMina(celda);
                            }

                        }

                    } else if (celda.estaActivada) {

                        if (celda.EsBandera()) { // ha perdido
                            celda.BackColor = Color.Crimson;
                        }

                    }
                }
            }
            

        }

        public void JugarCelda( int i, int j ) {
            Celda c = tabMinas[i][j];

            if (c.esMina) {
                FinalizarPartida(false);
                c.BackColor = Color.Black;

                if (idioma == "English") {
                    MessageBox.Show("You lost, it was a mine.", "Game Over", MessageBoxButtons.OK, MessageBoxIcon.Error);
                } else if (idioma == "Spanish") {
                    MessageBox.Show("Has perdido, era una mina.", "Partida Finalizada", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                return;
            }

            Revelar(i, j);
            RevelarAlrededor(i, j);

            if (celdasDescubiertas >= celdasNoMinas) {
                FinalizarPartida(true);
                progressBarMinas.Value = minas;

                NuevoResultado.ComprobarPartida(nivel, segundos);

                return;
            }

        }

        public void Casilla_Click( object sender, EventArgs e ) {
            if (finalizado) {
                panelTablero.Focus();
                return;
            }

            MouseEventArgs me = (MouseEventArgs) e;
            Celda accionador = (Celda) sender;

            if (me.Button == MouseButtons.Left) {

                if (accionador.EsBandera()) {
                    panelTablero.Focus();
                    return;
                }

                if (primerClick) {
                    primerClick = false;
                    RepartirMinas(accionador);
                }

                JugarCelda(accionador.x, accionador.y);

                if (trucos) {
                    ActivarTrucos();
                }

            } else if (me.Button == MouseButtons.Right) {

                if (accionador.estaActivada) {
                    if (accionador.EsBandera()) {
                        //accionador.Text = "";
                        accionador.BackgroundImage = null;

                        minasRestantes++;
                    } else {
                        PonerBandera(accionador);

                        minasRestantes--;
                    }
                    CambiarMarcador();
                }


            }

            panelTablero.Focus();
        }

        private void vbjvbnToolStripMenuItem_Click( object sender, EventArgs e ) {

        }

        private void salirToolStripMenuItem_Click( object sender, EventArgs e ) {
            Close();
        }

        private void CambiarNivel() {

            if (primerClick || finalizado) {
                NuevaPartida();
            } else if (idioma == "Spanish") {

                if (MessageBox.Show("¿Desea empezar una partida nueva?", "Configuración cambiada", MessageBoxButtons.YesNo) == DialogResult.Yes) {
                    NuevaPartida();
                }

            } else if (idioma == "English") {

                if (MessageBox.Show("Do you want to start a new game?", "Change level", MessageBoxButtons.YesNo) == DialogResult.Yes) {
                    NuevaPartida();
                }

            }

        }

        private void menuFacil_Click( object sender, EventArgs e ) {
            nivel = "Easy";

            CambiarNivel();

            Program.EscribirRegistro(Program.seccionNivel, nivel);
        }

        private void menuMejoresTiempos_Click( object sender, EventArgs e ) {

            Resultados resultados = new Resultados();
            resultados.ShowDialog();

        }

        private void menuIntermedio_Click( object sender, EventArgs e ) {
            nivel = "Regular";

            CambiarNivel();

            Program.EscribirRegistro(Program.seccionNivel, nivel);
        }

        private void menuDificil_Click( object sender, EventArgs e ) {
            nivel = "Hard";

            CambiarNivel();

            Program.EscribirRegistro(Program.seccionNivel, nivel);
        }

        private void menuEspañol_Click( object sender, EventArgs e ) {
            idioma = "Spanish";
            SetLanguage();

            Program.EscribirRegistro(Program.seccionIdioma, idioma);
        }

        private void menuIngles_Click( object sender, EventArgs e ) {
            idioma = "English";
            SetLanguage();

            Program.EscribirRegistro(Program.seccionIdioma, idioma);
        }

        public void PonerBandera( Celda c ) {
            c.BackgroundImage = ( (System.Drawing.Image) ( resources.GetObject("button2.BackgroundImage") ) );
            c.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
        }

        public void PonerMina( Celda c ) {
            c.BackgroundImage = ( (System.Drawing.Image) ( resources.GetObject("button1.BackgroundImage") ) );
            c.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
        }

        private void MinesWeeper_KeyUp( object sender, KeyEventArgs e ) {
            Keys k = e.KeyCode;

            if (k == Keys.C) {
                trucosSecuencia = true;
                ultimaLetra = k;
            } else if (k == Keys.H && ultimaLetra == Keys.C && trucosSecuencia) {
                ultimaLetra = k;
            } else if (k == Keys.E && ultimaLetra == Keys.H && trucosSecuencia) {
                ultimaLetra = k;
            } else if (k == Keys.A && ultimaLetra == Keys.E && trucosSecuencia) {
                ultimaLetra = k;
            } else if (k == Keys.T && ultimaLetra == Keys.A && trucosSecuencia) {
                trucos = !trucos;
                ActivarTrucos();
            } else {
                trucosSecuencia = false;
            }

        }

        private void menuEstiloNormal_Click( object sender, EventArgs e ) {
            estilo = "Normal";
            SetEstilo();

            Program.EscribirRegistro(Program.seccionEstilo, estilo);
        }

        private void menuEstiloBasico_Click( object sender, EventArgs e ) {
            estilo = "Basic";
            SetEstilo();

            Program.EscribirRegistro(Program.seccionEstilo, estilo);
        }

        private void timer1_Tick( object sender, EventArgs e ) {

            if (!primerClick && !finalizado) {
                tiempoLabel.Text = "" + ( ++segundos );
            }

        }

        private void reiniciarBoton_Click( object sender, EventArgs e ) {
            NuevaPartida();
        }

        private void menuNuevaPartida_Click( object sender, EventArgs e ) {
            NuevaPartida();
        }

    }
}