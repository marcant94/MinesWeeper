﻿using System;

namespace Buscaminas {
    partial class MinesWeeper {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose( bool disposing ) {
            if (disposing && ( components != null )) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MinesWeeper));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuJuego = new System.Windows.Forms.ToolStripMenuItem();
            this.menuNuevaPartida = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.menuFacil = new System.Windows.Forms.ToolStripMenuItem();
            this.menuIntermedio = new System.Windows.Forms.ToolStripMenuItem();
            this.menuDificil = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.menuMejoresTiempos = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.menuSalir = new System.Windows.Forms.ToolStripMenuItem();
            this.menuIdioma = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEspañol = new System.Windows.Forms.ToolStripMenuItem();
            this.menuIngles = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEstilo = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEstiloNormal = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEstiloBasico = new System.Windows.Forms.ToolStripMenuItem();
            this.progressBarMinas = new System.Windows.Forms.ProgressBar();
            this.minasRestantesLabel = new System.Windows.Forms.Label();
            this.tiempoLabel = new System.Windows.Forms.Label();
            this.reiniciarBoton = new System.Windows.Forms.Button();
            this.panelTablero = new System.Windows.Forms.FlowLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.timerSegundo = new System.Windows.Forms.Timer(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            this.panelTablero.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuJuego,
            this.menuIdioma,
            this.menuEstilo});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.menuStrip1.Size = new System.Drawing.Size(585, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuJuego
            // 
            this.menuJuego.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuNuevaPartida,
            this.toolStripMenuItem3,
            this.menuFacil,
            this.menuIntermedio,
            this.menuDificil,
            this.toolStripMenuItem1,
            this.menuMejoresTiempos,
            this.toolStripMenuItem2,
            this.menuSalir});
            this.menuJuego.Name = "menuJuego";
            this.menuJuego.Size = new System.Drawing.Size(60, 20);
            this.menuJuego.Text = "B.Juego";
            this.menuJuego.Click += new System.EventHandler(this.vbjvbnToolStripMenuItem_Click);
            // 
            // menuNuevaPartida
            // 
            this.menuNuevaPartida.Name = "menuNuevaPartida";
            this.menuNuevaPartida.Size = new System.Drawing.Size(174, 22);
            this.menuNuevaPartida.Text = "B.Nueva Partida";
            this.menuNuevaPartida.Click += new System.EventHandler(this.menuNuevaPartida_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(171, 6);
            // 
            // menuFacil
            // 
            this.menuFacil.Checked = true;
            this.menuFacil.CheckState = System.Windows.Forms.CheckState.Checked;
            this.menuFacil.Name = "menuFacil";
            this.menuFacil.Size = new System.Drawing.Size(174, 22);
            this.menuFacil.Text = "B.Facil";
            this.menuFacil.Click += new System.EventHandler(this.menuFacil_Click);
            // 
            // menuIntermedio
            // 
            this.menuIntermedio.Name = "menuIntermedio";
            this.menuIntermedio.Size = new System.Drawing.Size(174, 22);
            this.menuIntermedio.Text = "B.Intermedio";
            this.menuIntermedio.Click += new System.EventHandler(this.menuIntermedio_Click);
            // 
            // menuDificil
            // 
            this.menuDificil.Name = "menuDificil";
            this.menuDificil.Size = new System.Drawing.Size(174, 22);
            this.menuDificil.Text = "B.Dificil";
            this.menuDificil.Click += new System.EventHandler(this.menuDificil_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(171, 6);
            // 
            // menuMejoresTiempos
            // 
            this.menuMejoresTiempos.Name = "menuMejoresTiempos";
            this.menuMejoresTiempos.Size = new System.Drawing.Size(174, 22);
            this.menuMejoresTiempos.Text = "B.Mejores Tiempos";
            this.menuMejoresTiempos.Click += new System.EventHandler(this.menuMejoresTiempos_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(171, 6);
            // 
            // menuSalir
            // 
            this.menuSalir.Name = "menuSalir";
            this.menuSalir.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.menuSalir.Size = new System.Drawing.Size(174, 22);
            this.menuSalir.Text = "B.Salir";
            this.menuSalir.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // menuIdioma
            // 
            this.menuIdioma.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuEspañol,
            this.menuIngles});
            this.menuIdioma.Name = "menuIdioma";
            this.menuIdioma.Size = new System.Drawing.Size(66, 20);
            this.menuIdioma.Text = "B.Idioma";
            // 
            // menuEspañol
            // 
            this.menuEspañol.Checked = true;
            this.menuEspañol.CheckState = System.Windows.Forms.CheckState.Checked;
            this.menuEspañol.Name = "menuEspañol";
            this.menuEspañol.Size = new System.Drawing.Size(125, 22);
            this.menuEspañol.Text = "B.Español";
            this.menuEspañol.Click += new System.EventHandler(this.menuEspañol_Click);
            // 
            // menuIngles
            // 
            this.menuIngles.Name = "menuIngles";
            this.menuIngles.Size = new System.Drawing.Size(125, 22);
            this.menuIngles.Text = "B.Inglés";
            this.menuIngles.Click += new System.EventHandler(this.menuIngles_Click);
            // 
            // menuEstilo
            // 
            this.menuEstilo.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuEstiloNormal,
            this.menuEstiloBasico});
            this.menuEstilo.Name = "menuEstilo";
            this.menuEstilo.Size = new System.Drawing.Size(57, 20);
            this.menuEstilo.Text = "B.Estilo";
            // 
            // menuEstiloNormal
            // 
            this.menuEstiloNormal.Name = "menuEstiloNormal";
            this.menuEstiloNormal.Size = new System.Drawing.Size(124, 22);
            this.menuEstiloNormal.Text = "B.Normal";
            this.menuEstiloNormal.Click += new System.EventHandler(this.menuEstiloNormal_Click);
            // 
            // menuEstiloBasico
            // 
            this.menuEstiloBasico.Name = "menuEstiloBasico";
            this.menuEstiloBasico.Size = new System.Drawing.Size(124, 22);
            this.menuEstiloBasico.Text = "B.Basico";
            this.menuEstiloBasico.Click += new System.EventHandler(this.menuEstiloBasico_Click);
            // 
            // progressBarMinas
            // 
            this.progressBarMinas.Location = new System.Drawing.Point(123, 31);
            this.progressBarMinas.Name = "progressBarMinas";
            this.progressBarMinas.Size = new System.Drawing.Size(110, 27);
            this.progressBarMinas.TabIndex = 2;
            this.progressBarMinas.Value = 50;
            // 
            // minasRestantesLabel
            // 
            this.minasRestantesLabel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.minasRestantesLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.minasRestantesLabel.Font = new System.Drawing.Font("Times New Roman", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.minasRestantesLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.minasRestantesLabel.Location = new System.Drawing.Point(54, 30);
            this.minasRestantesLabel.Name = "minasRestantesLabel";
            this.minasRestantesLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.minasRestantesLabel.Size = new System.Drawing.Size(63, 28);
            this.minasRestantesLabel.TabIndex = 3;
            this.minasRestantesLabel.Text = "020";
            this.minasRestantesLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tiempoLabel
            // 
            this.tiempoLabel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tiempoLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tiempoLabel.Font = new System.Drawing.Font("Times New Roman", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.tiempoLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.tiempoLabel.Location = new System.Drawing.Point(457, 29);
            this.tiempoLabel.Name = "tiempoLabel";
            this.tiempoLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tiempoLabel.Size = new System.Drawing.Size(72, 27);
            this.tiempoLabel.TabIndex = 4;
            this.tiempoLabel.Text = "0000";
            this.tiempoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // reiniciarBoton
            // 
            this.reiniciarBoton.CausesValidation = false;
            this.reiniciarBoton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.reiniciarBoton.Location = new System.Drawing.Point(314, 29);
            this.reiniciarBoton.Name = "reiniciarBoton";
            this.reiniciarBoton.Size = new System.Drawing.Size(75, 28);
            this.reiniciarBoton.TabIndex = 5;
            this.reiniciarBoton.TabStop = false;
            this.reiniciarBoton.Text = "B.Reiniciar";
            this.reiniciarBoton.UseVisualStyleBackColor = true;
            this.reiniciarBoton.Click += new System.EventHandler(this.reiniciarBoton_Click);
            // 
            // panelTablero
            // 
            this.panelTablero.Controls.Add(this.button1);
            this.panelTablero.Controls.Add(this.button2);
            this.panelTablero.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelTablero.Location = new System.Drawing.Point(9, 72);
            this.panelTablero.Margin = new System.Windows.Forms.Padding(0);
            this.panelTablero.Name = "panelTablero";
            this.panelTablero.Size = new System.Drawing.Size(567, 473);
            this.panelTablero.TabIndex = 6;
            // 
            // button1
            // 
            this.button1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.button1.BackColor = System.Drawing.SystemColors.Control;
            this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button1.ForeColor = System.Drawing.Color.RoyalBlue;
            this.button1.Location = new System.Drawing.Point(3, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(163, 95);
            this.button1.TabIndex = 0;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Gainsboro;
            this.button2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button2.BackgroundImage")));
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button2.Cursor = System.Windows.Forms.Cursors.No;
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button2.FlatAppearance.BorderSize = 5;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button2.ForeColor = System.Drawing.SystemColors.InfoText;
            this.button2.Location = new System.Drawing.Point(172, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(163, 95);
            this.button2.TabIndex = 1;
            this.button2.Text = "gfhfghfh";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // timerSegundo
            // 
            this.timerSegundo.Enabled = true;
            this.timerSegundo.Interval = 1000;
            this.timerSegundo.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(535, 28);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(39, 28);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(9, 29);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(38, 28);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 8;
            this.pictureBox2.TabStop = false;
            // 
            // MinesWeeper
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(585, 554);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panelTablero);
            this.Controls.Add(this.reiniciarBoton);
            this.Controls.Add(this.tiempoLabel);
            this.Controls.Add(this.minasRestantesLabel);
            this.Controls.Add(this.progressBarMinas);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.MaximizeBox = false;
            this.Name = "MinesWeeper";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Buscaminas Debug";
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MinesWeeper_KeyUp);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panelTablero.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuJuego;
        private System.Windows.Forms.ToolStripMenuItem menuFacil;
        private System.Windows.Forms.ToolStripMenuItem menuIntermedio;
        private System.Windows.Forms.ToolStripMenuItem menuMejoresTiempos;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem menuSalir;
        private System.Windows.Forms.ToolStripMenuItem menuIdioma;
        private System.Windows.Forms.ToolStripMenuItem menuEspañol;
        private System.Windows.Forms.ToolStripMenuItem menuIngles;
        private System.Windows.Forms.ToolStripMenuItem menuNuevaPartida;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ProgressBar progressBarMinas;
        private System.Windows.Forms.Label minasRestantesLabel;
        private System.Windows.Forms.Label tiempoLabel;
        private System.Windows.Forms.Button reiniciarBoton;
        private System.Windows.Forms.FlowLayoutPanel panelTablero;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Timer timerSegundo;
        private System.Windows.Forms.ToolStripMenuItem menuEstilo;
        private System.Windows.Forms.ToolStripMenuItem menuEstiloNormal;
        private System.Windows.Forms.ToolStripMenuItem menuEstiloBasico;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.ToolStripMenuItem menuDificil;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
    }

}

