﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;

namespace Buscaminas {
    public class Celda : Button {
        public int x, y;
        public int minasAlrededor;
        public Boolean esMina, estaActivada;

        public Celda(int x, int y) {
            this.x = x;
            this.y = y;
            esMina = false;
            minasAlrededor = 0;
            estaActivada = true;

            int fontSize = 1;
            if (MinesWeeper.nivel == "Easy") {
                fontSize = 17;
            } else if (MinesWeeper.nivel == "Regular") {
                fontSize = 14;
            } else if (MinesWeeper.nivel == "Hard") {
                fontSize = 11;
            }

            BackColor = Color.Gainsboro;

            FlatAppearance.BorderColor = SystemColors.ControlDark;
            FlatAppearance.BorderSize = 1;

            TabStop = false;
            Font = new System.Drawing.Font("Microsoft Sans Serif", fontSize, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ));
            
        }

        public void PulsarCelda() {
            estaActivada = false;
            BackColor = SystemColors.Control;
        }

        public void MinaAlrededor() {
            minasAlrededor++;
        }

        public bool EsBandera() {
            if (BackgroundImage != null) {
                return true;
            } else {
                return false;
            }
        }

    }


}
