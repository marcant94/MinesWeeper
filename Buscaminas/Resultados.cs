﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Buscaminas {
    public partial class Resultados : Form {
        public static string registroFacil = "Easy";
        public static string registroNormal = "Regular";
        public static string registroDificil = "Hard";

        public static string registroNombreFacil = "NameEasy";
        public static string registroNombreNormal = "NameRegular";
        public static string registroNombreDificil = "NameHard";

        public Resultados() {
            InitializeComponent();

            InicializarVentana();
            RecopilarMostrarDatos();

        }

        public void InicializarVentana() {
            string idioma = MinesWeeper.idioma;

            if (idioma == "Spanish") {
                Text = "Mejores Resultados";
                buttonOK.Text = "Aceptar";
                buttonRestart.Text = "Reiniciar tiempos";

                labelFacil.Text = "Principiante";
                labelNormal.Text = "Intermedio";
                labelDificil.Text = "Experto";

                nombreFacil.Text = "Anónimo";
                nombreNormal.Text = "Anónimo";
                nombreDificil.Text = "Anónimo";

            } else {
                Text = "Top Results";
                buttonOK.Text = "OK";
                buttonRestart.Text = "Restart times";

                labelFacil.Text = "Beginner";
                labelNormal.Text = "Medium";
                labelDificil.Text = "Expert";

                nombreFacil.Text = "Anonymous";
                nombreNormal.Text = "Anonymous";
                nombreDificil.Text = "Anonymous";

            }

        }

        public void RecopilarMostrarDatos() {
            string idioma = MinesWeeper.idioma;

            string facil = Program.LeerRegistro(registroFacil, "Null");
            string normal = Program.LeerRegistro(registroNormal, "Null");
            string dificil = Program.LeerRegistro(registroDificil, "Null");

            if (facil == "Null") {
                resultadoFacil.Text = "-";
            } else {
                resultadoFacil.Text = facil;
            }

            if (normal == "Null") {
                resultadoNormal.Text = "-";
            } else {
                resultadoNormal.Text = normal;
            }

            if (dificil == "Null") {
                resultadoDificil.Text = "-";
            } else {
                resultadoDificil.Text = dificil;
            }

            string sNombreFacil = Program.LeerRegistro(registroNombreFacil, "Null");
            string sNombreNormal = Program.LeerRegistro(registroNombreNormal, "Null");
            string sNombreDificil = Program.LeerRegistro(registroNombreDificil, "Null");

            if (sNombreFacil == "Null") {
                if (idioma == "Spanish") {
                    nombreFacil.Text = "Anónimo";
                } else {
                    nombreFacil.Text = "Anonymous";
                }
            } else {
                nombreFacil.Text = sNombreFacil;
            }

            if (sNombreNormal == "Null") {
                if (idioma == "Spanish") {
                    nombreNormal.Text = "Anónimo";
                } else {
                    nombreNormal.Text = "Anonymous";
                }
            } else {
                nombreNormal.Text = sNombreNormal;
            }

            if (sNombreDificil == "Null") {
                if (idioma == "Spanish") {
                    nombreDificil.Text = "Anónimo";
                } else {
                    nombreDificil.Text = "Anonymous";
                }
            } else {
                nombreDificil.Text = sNombreDificil;
            }

        }

        private void Resultados_Load( object sender, EventArgs e ) {

        }

        private void buttonOK_Click( object sender, EventArgs e ) {
            Close();
        }

        private void buttonRestart_Click( object sender, EventArgs e ) {

            Program.EscribirRegistro(registroFacil, "Null");
            Program.EscribirRegistro(registroNormal, "Null");
            Program.EscribirRegistro(registroDificil, "Null");

            Program.EscribirRegistro(registroNombreFacil, "Null");
            Program.EscribirRegistro(registroNombreNormal, "Null");
            Program.EscribirRegistro(registroNombreDificil, "Null");

            InicializarVentana();

            resultadoFacil.Text = "-";
            resultadoNormal.Text = "-";
            resultadoDificil.Text = "-";

        }

    }
}
