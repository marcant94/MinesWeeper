﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Buscaminas {
    public partial class SelectLanguage : Form {
        public string language;

        public SelectLanguage() {
            InitializeComponent();
            comboBoxLanguage.SelectedIndex = 0;
        }



        private void textBox1_TextChanged( object sender, EventArgs e ) {

        }

        private void domainUpDown1_SelectedItemChanged( object sender, EventArgs e ) {
            
        }

        private void comboBoxLanguage_SelectedIndexChanged( object sender, EventArgs e ) {

        }

        private void SelectLanguage_Load( object sender, EventArgs e ) {

        }

        private void buttonOkIdioma_Click( object sender, EventArgs e ) {
            string idioma = comboBoxLanguage.Text;

            if (idioma == "Español") {
                language = "Spanish";
            } else {
                language = "English";
            }

            Close();
        }
    }
}
