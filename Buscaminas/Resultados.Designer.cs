﻿namespace Buscaminas {
    partial class Resultados {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing ) {
            if (disposing && ( components != null )) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Resultados));
            this.labelFacil = new System.Windows.Forms.Label();
            this.resultadoFacil = new System.Windows.Forms.Label();
            this.nombreFacil = new System.Windows.Forms.Label();
            this.nombreNormal = new System.Windows.Forms.Label();
            this.resultadoNormal = new System.Windows.Forms.Label();
            this.labelNormal = new System.Windows.Forms.Label();
            this.nombreDificil = new System.Windows.Forms.Label();
            this.resultadoDificil = new System.Windows.Forms.Label();
            this.labelDificil = new System.Windows.Forms.Label();
            this.buttonRestart = new System.Windows.Forms.Button();
            this.buttonOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelFacil
            // 
            this.labelFacil.AutoSize = true;
            this.labelFacil.Location = new System.Drawing.Point(26, 33);
            this.labelFacil.Name = "labelFacil";
            this.labelFacil.Size = new System.Drawing.Size(43, 15);
            this.labelFacil.TabIndex = 1;
            this.labelFacil.Text = "L. Facil";
            this.labelFacil.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // resultadoFacil
            // 
            this.resultadoFacil.Location = new System.Drawing.Point(113, 33);
            this.resultadoFacil.Name = "resultadoFacil";
            this.resultadoFacil.Size = new System.Drawing.Size(98, 13);
            this.resultadoFacil.TabIndex = 2;
            this.resultadoFacil.Text = "L. R f";
            this.resultadoFacil.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // nombreFacil
            // 
            this.nombreFacil.Location = new System.Drawing.Point(234, 33);
            this.nombreFacil.Name = "nombreFacil";
            this.nombreFacil.Size = new System.Drawing.Size(135, 13);
            this.nombreFacil.TabIndex = 3;
            this.nombreFacil.Text = "L. N f";
            this.nombreFacil.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // nombreNormal
            // 
            this.nombreNormal.Location = new System.Drawing.Point(234, 77);
            this.nombreNormal.Name = "nombreNormal";
            this.nombreNormal.Size = new System.Drawing.Size(135, 13);
            this.nombreNormal.TabIndex = 6;
            this.nombreNormal.Text = "L. N n";
            this.nombreNormal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // resultadoNormal
            // 
            this.resultadoNormal.Location = new System.Drawing.Point(113, 77);
            this.resultadoNormal.Name = "resultadoNormal";
            this.resultadoNormal.Size = new System.Drawing.Size(101, 13);
            this.resultadoNormal.TabIndex = 5;
            this.resultadoNormal.Text = "L. R n";
            this.resultadoNormal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelNormal
            // 
            this.labelNormal.AutoSize = true;
            this.labelNormal.Location = new System.Drawing.Point(26, 77);
            this.labelNormal.Name = "labelNormal";
            this.labelNormal.Size = new System.Drawing.Size(59, 15);
            this.labelNormal.TabIndex = 4;
            this.labelNormal.Text = "L. Normal";
            this.labelNormal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // nombreDificil
            // 
            this.nombreDificil.Location = new System.Drawing.Point(234, 120);
            this.nombreDificil.Name = "nombreDificil";
            this.nombreDificil.Size = new System.Drawing.Size(135, 13);
            this.nombreDificil.TabIndex = 9;
            this.nombreDificil.Text = "L. N d";
            this.nombreDificil.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // resultadoDificil
            // 
            this.resultadoDificil.Location = new System.Drawing.Point(113, 120);
            this.resultadoDificil.Name = "resultadoDificil";
            this.resultadoDificil.Size = new System.Drawing.Size(101, 13);
            this.resultadoDificil.TabIndex = 8;
            this.resultadoDificil.Text = "L. R d";
            this.resultadoDificil.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelDificil
            // 
            this.labelDificil.AutoSize = true;
            this.labelDificil.Location = new System.Drawing.Point(26, 120);
            this.labelDificil.Name = "labelDificil";
            this.labelDificil.Size = new System.Drawing.Size(49, 15);
            this.labelDificil.TabIndex = 7;
            this.labelDificil.Text = "L. Dificil";
            this.labelDificil.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonRestart
            // 
            this.buttonRestart.Location = new System.Drawing.Point(48, 159);
            this.buttonRestart.Name = "buttonRestart";
            this.buttonRestart.Size = new System.Drawing.Size(136, 23);
            this.buttonRestart.TabIndex = 10;
            this.buttonRestart.Text = "B. RS";
            this.buttonRestart.UseVisualStyleBackColor = true;
            this.buttonRestart.Click += new System.EventHandler(this.buttonRestart_Click);
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(243, 159);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(87, 23);
            this.buttonOK.TabIndex = 11;
            this.buttonOK.Text = "B. OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // Resultados
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(395, 201);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.buttonRestart);
            this.Controls.Add(this.nombreDificil);
            this.Controls.Add(this.resultadoDificil);
            this.Controls.Add(this.labelDificil);
            this.Controls.Add(this.nombreNormal);
            this.Controls.Add(this.resultadoNormal);
            this.Controls.Add(this.labelNormal);
            this.Controls.Add(this.nombreFacil);
            this.Controls.Add(this.resultadoFacil);
            this.Controls.Add(this.labelFacil);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Resultados";
            this.Opacity = 0.9D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Resultados Debug";
            this.Load += new System.EventHandler(this.Resultados_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelFacil;
        private System.Windows.Forms.Label resultadoFacil;
        private System.Windows.Forms.Label nombreFacil;
        private System.Windows.Forms.Label nombreNormal;
        private System.Windows.Forms.Label resultadoNormal;
        private System.Windows.Forms.Label labelNormal;
        private System.Windows.Forms.Label nombreDificil;
        private System.Windows.Forms.Label resultadoDificil;
        private System.Windows.Forms.Label labelDificil;
        private System.Windows.Forms.Button buttonRestart;
        private System.Windows.Forms.Button buttonOK;
    }
}